#-------------------------------------------------
#
# Project created by QtCreator 2016-01-14T11:37:50
#
#-------------------------------------------------

QT       += core gui concurrent
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = concordance_gui_with_threads
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h

FORMS    += widget.ui
