#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QtConcurrent>


namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_pushButton_Load_clicked();
    void calculate();
    void update_stats();

private:
    Ui::Widget *ui;
    QFutureWatcher<QMap<int, QStringList>>* watcher;
    QMap<int, QStringList> calc_stats(QString split, QString filter, QString text);
};

#endif // WIDGET_H
