#include "widget.h"
#include "ui_widget.h"
#include <QFileDialog>
#include <QString>
#include <QDebug>
#include <QTextStream>
#include <QFile>
#include <QHash>
#include <QMap>
#include <QTime>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    watcher = new QFutureWatcher<QMap<int, QStringList>>(this);

    ui->setupUi(this);
    connect(ui->plainTextEdit, SIGNAL(textChanged()),
            this, SLOT(calculate()));
    connect(ui->leFilter, SIGNAL(textChanged()),
            this, SLOT(calculate()));
    connect(ui->leSplit, SIGNAL(textChanged()),
            this, SLOT(calculate()));
    connect(watcher, SIGNAL(finished()),
            this, SLOT(update_stats()));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_Load_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    "Open file for concordance",
                                                    ".",
                                                    "All files (*.*);;Text (*.txt)");

    qDebug() << filename;
    if (!filename.isEmpty())
    {
        QFile f(filename);
        if (f.open(QFile::ReadOnly | QFile::Text))
        {
            QTextStream in(&f);
            ui->plainTextEdit->setPlainText( f.readAll() );
        }
    }
    //calculate();
}

void Widget::calculate()
{
    QString split_txt = ui->leSplit->text();
    QString filter_txt = ui->leFilter->text();
    QString text = ui->plainTextEdit->toPlainText();
    watcher->cancel();
    watcher->setFuture(QtConcurrent::run(this, &Widget::calc_stats, split_txt, filter_txt, text));
}

void Widget::update_stats()   // main thread method
{
    QMap<int, QStringList> result = watcher->result();

    QMapIterator<int, QStringList> rit(result);
    rit.toBack();
    int i = 0;
    ui->tableWidget->clearContents();
    ui->tableWidget->setRowCount(30);
    while(rit.hasPrevious() && i < 30)
    {
        ++i;
        rit.previous();
        //qDebug() << i << rit.key() << rit.value();
        ui->tableWidget->setItem(i, 0, new QTableWidgetItem(QString::number(rit.key())));
        ui->tableWidget->setItem(i, 1, new QTableWidgetItem(rit.value().join(", ")));
    }
}

QMap<int, QStringList> Widget::calc_stats(QString split, QString filter, QString text)
{
    qDebug() << "started";
    QTime timer;
    timer.start();
    QStringList words =
           text.split(QRegExp(split)).filter(QRegExp(filter));
    QHash<QString, int> hfreq;
    for(const auto& word : words)
    {
        hfreq[word.toLower()]++;
    }

    QMap<int, QStringList> result;
    QHashIterator<QString, int> it(hfreq);
    while(it.hasNext())
    {
        it.next();
        result[it.value()] << it.key();
    }
    qDebug() << "finished" << timer.elapsed();
    return result;
}
