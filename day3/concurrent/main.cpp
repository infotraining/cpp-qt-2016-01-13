#include <QCoreApplication>
#include <QtConcurrent>
#include <QtDebug>
#include <QString>
#include <QThread>
#include <QThreadPool>
#include <QFutureWatcher>
#include <QList>

void hello()
{
    qDebug() << "hello from" << QThread::currentThreadId();
}

int answer(QString text)
{
    QThread::sleep(2);
    qDebug() << text;
    return 42;
}

void sqr(int& a)
{
    a = a*a;
}

int sqr2(int a)
{
    return a*a;
}

bool is_odd(int a)
{
    return a % 2;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QThreadPool p;
    p.setMaxThreadCount(10);
    QFutureWatcher<int> w;
    QObject::connect(&w, &QFutureWatcher<int>::finished,
                     [&w] () { qDebug() << w.result(); });

    qDebug() << "main from" << QThread::currentThreadId();
    QFuture<void> res = QtConcurrent::run(hello);
    res.waitForFinished();

    auto resint1 = QtConcurrent::run(&p, answer, QString("What is the meaning of all?"));
    w.setFuture(resint1);

    QList<int> cont;
    for (int i = 0 ; i < 20 ; ++i)
        cont << i;

    qDebug() << cont;

    QFuture<void> fut = QtConcurrent::map(cont, sqr);
    fut.waitForFinished();
    qDebug() << cont;


    QFutureWatcher<int> w2;
    w2.setFuture(QtConcurrent::mapped(cont, sqr2));
    QObject::connect(&w2, &QFutureWatcher<int>::resultReadyAt,
            [&] (int i) { qDebug() << w2.resultAt(i); });

    QList<int> cont2;
    for (int i = 0 ; i < 20 ; ++i)
        cont2 << i;

    QFuture<void> ffil = QtConcurrent::filter(cont2, is_odd);
    ffil.waitForFinished();
    qDebug() << cont2;


    return a.exec();
}
