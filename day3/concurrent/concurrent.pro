QT += core concurrent
QT -= gui

CONFIG += c++14

TARGET = concurrent
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp
