#include <QCoreApplication>
#include <QDebug>
#include "mythread.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    MyThread th1("moj watek 1");
    QObject::connect(&th1, &MyThread::finished,
                     [&] () { qDebug() << "thread has finished";
                             a.quit();});
    th1.start();

    return a.exec();
}

