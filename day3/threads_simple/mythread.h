#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QObject>
#include <QThread>
#include <QString>

class MyThread : public QThread
{
    Q_OBJECT
    QString name_;
public:
    MyThread(QString name, QObject* parent = 0);
    //void run(); Q_OVERRIDE() // pre c++11
protected:
    void run() override;   // c++11
};

#endif // MYTHREAD_H
