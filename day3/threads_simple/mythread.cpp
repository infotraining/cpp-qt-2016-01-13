#include "mythread.h"
#include <QDebug>

MyThread::MyThread(QString name, QObject* parent)
    : QThread(parent), name_(name)
{

}

void MyThread::run()
{
    qDebug() << name_ << "thread has started";
    QThread::sleep(1);
    qDebug() << "n of cores " << QThread::idealThreadCount();
}
