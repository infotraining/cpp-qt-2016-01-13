#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "operation.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT
    Operation* worker;
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

public slots:
    void op_start();
    void op_cancel();

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
