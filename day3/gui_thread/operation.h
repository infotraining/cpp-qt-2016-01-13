#ifndef OPERATION_H
#define OPERATION_H

#include <QObject>
#include <QThread>
#include <QMutex>
#include <QMutexLocker>

class Operation : public QThread
{
    Q_OBJECT
    bool abort_;
    QMutex mtx;
public:
    explicit Operation(QObject *parent = 0);

signals:
    void notifyProgress(int);

public slots:    
    void abort();

protected:
    void run() override;
};

#endif // OPERATION_H
