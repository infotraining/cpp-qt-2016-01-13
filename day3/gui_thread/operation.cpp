#include "operation.h"

Operation::Operation(QObject *parent)
    : QThread(parent), abort_(false)
{

}

void Operation::abort()
{
    QMutexLocker l(&mtx);
    abort_ = true;
}

void Operation::run()
{
    for (int i = 1 ; i < 100 ; ++i)
    {
        {
            QMutexLocker l(&mtx);
            if(abort_)
            {
                abort_ = false;
                return;
            }
        }
        msleep(50);
        emit notifyProgress(i);
    }
    emit notifyProgress(100);
}
