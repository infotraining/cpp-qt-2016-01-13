#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    ui->pbCancel->setDisabled(true);

    worker = new Operation(this);

    connect(ui->pbStart, SIGNAL(clicked(bool)),
            this, SLOT(op_start()));

    connect(worker, SIGNAL(notifyProgress(int)),
            ui->progressBar, SLOT(setValue(int)));

    connect(ui->pbCancel, SIGNAL(clicked(bool)),
            this, SLOT(op_cancel()));


}

Widget::~Widget()
{
    delete ui;
}

void Widget::op_start()
{
    ui->pbStart->setDisabled(true);
    worker->start();
    ui->pbCancel->setEnabled(true);
}

void Widget::op_cancel()
{
    worker->abort();
    ui->pbStart->setEnabled(true);
    ui->pbCancel->setDisabled(true);
}
