#-------------------------------------------------
#
# Project created by QtCreator 2016-01-15T09:39:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = gui_thread
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    operation.cpp

HEADERS  += widget.h \
    operation.h

FORMS    += widget.ui
