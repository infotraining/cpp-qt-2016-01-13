#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QtNetwork>
#include <QStringList>

class Server : public QObject
{
    Q_OBJECT

    QTcpServer* tcp_server;
    QStringList messages;
public:
    explicit Server(QObject *parent = 0);

signals:

public slots:
    void StartServer();
    void SendMessage();
};

#endif // SERVER_H
