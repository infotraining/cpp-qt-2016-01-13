#include "server.h"
#include <QDebug>

Server::Server(QObject *parent) : QObject(parent)
{
    tcp_server = new QTcpServer(this);
    messages << "hello 1" << "hello 2";
    connect(tcp_server, SIGNAL(newConnection()),
            this, SLOT(SendMessage()));
}

void Server::StartServer()
{
    if (!tcp_server->listen(QHostAddress::Any, 1234))
    {
        qDebug() << "something is wrong";
        qDebug() << tcp_server->errorString();
    }
}

void Server::SendMessage()
{

    // message preparation
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << (qint32)0;
    out << messages.at(qrand() % messages.size());
    out.device()->seek(0);
    out << (qint32)(block.size() - sizeof(qint32));
    // message preparation

    qDebug() << "send message";
    QTcpSocket *client;
    client = tcp_server->nextPendingConnection();
    QByteArray block;


    connect(client, SIGNAL(disconnected()),
            client, SLOT(deleteLater()));
    client->write(block);
    client->disconnectFromHost();

}
