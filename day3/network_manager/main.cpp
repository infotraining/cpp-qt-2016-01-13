#include <QCoreApplication>
#include <QtNetwork>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QNetworkAccessManager manager;
    QNetworkRequest req(QString("http://www.pesa.pl"));
    QNetworkReply *repl = manager.get(req);

    QObject::connect(repl, &QNetworkReply::finished,
                     [&] () { qDebug() << QString::fromUtf8(repl->readAll());});

    return a.exec();
}
