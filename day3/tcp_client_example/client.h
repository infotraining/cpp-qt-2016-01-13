#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QtNetwork>
#include <QString>
#include <QDebug>
#include <QDataStream>

class Client : public QObject
{
    Q_OBJECT
public:
    explicit Client(QObject *parent = 0);

signals:

public slots:
    void requestMessage();
    void readMessage();
    void finish();
private:
    qint32 block_size;
    QTcpSocket* socket;
};

#endif // CLIENT_H
