#include "client.h"

Client::Client(QObject *parent) : QObject(parent), block_size(0)
{
    socket = new QTcpSocket(this);
    connect(socket, SIGNAL(readyRead()),
            this, SLOT(readMessage()));
    connect(socket, SIGNAL(disconnected()),
            this, SLOT(finish()));
}

void Client::requestMessage()
{
    block_size = 0;
    socket->connectToHost("127.0.0.1", 1234);
}

void Client::readMessage()
{
    QDataStream in(socket);
    in.setVersion(QDataStream::Qt_5_5);

    if(block_size == 0) // first read
    {
        if(socket->bytesAvailable() < (int)sizeof(qint32))
            return;
        in >> block_size;
    }

    if(socket->bytesAvailable() < block_size)
        return;

    QString message;
    in >> message;
    qDebug() << "got message " << message;
}

void Client::finish()
{
    qDebug() << "finished";
    requestMessage();
}


