#include <QCoreApplication>
#include <QFile>
#include <QDebug>
#include <QString>
#include <QTextStream>
#include <QTime>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QFile file ("C:/workspace/swann.txt");

    if (!file.open(QIODevice::ReadOnly))
    {
        qDebug() << "Error opening file";
        return -1;
    }

    QTextStream stream(&file);
    QString all = stream.readAll();
    //qDebug() << all;
    QStringList words = all.split(QRegExp("\\W")).filter(QRegExp("\\w{2,}"));
    qDebug() << words.length();

    QTime timer;
//    timer.start();
//    QMap<QString, int> freq;
//    for(const auto& word : words)
//    {
//        freq[word.toLower()]++;
//    }
//    qDebug() << "QMap Time = " << timer.elapsed() << " ms";

    timer.start();
    QHash<QString, int> hfreq;
    for(const auto& word : words)
    {
        hfreq[word.toLower()]++;
    }
    qDebug() << "QHash Time = " << timer.elapsed() << " ms";

    QMap<int, QStringList> result;
    QHashIterator<QString, int> it(hfreq);
    while(it.hasNext())
    {
        it.next();
        result[it.value()] << it.key();
    }

    //qDebug() << result;

    QMapIterator<int, QStringList> rit(result);
    rit.toBack();
    int i = 0;
    while(rit.hasPrevious() && i < 100)
    {
        ++i;
        rit.previous();
        qDebug() << i << rit.key() << rit.value();
    }

    return a.exec();
}
