#include "myclassicclass.h"
#include <QDebug>

MyClassicClass::MyClassicClass(QString text)
    : text_(text)
{
    qDebug() << "ctor of " << text_;
}

MyClassicClass::~MyClassicClass()
{
    qDebug() << "dtor of " << text_;
}

void MyClassicClass::do_something()
{
    qDebug() << "hello " << text_;
}
