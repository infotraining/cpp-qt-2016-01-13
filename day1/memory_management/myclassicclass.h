#ifndef MYCLASSICCLASS_H
#define MYCLASSICCLASS_H

#include <QString>

class MyClassicClass
{
    QString text_;
public:
    MyClassicClass(QString text);
    ~MyClassicClass();
    void do_something();

};

#endif // MYCLASSICCLASS_H
