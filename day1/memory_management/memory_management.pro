QT += core
QT -= gui

CONFIG += c++14

TARGET = memory_management
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    myclassicclass.cpp \
    myqclass.cpp

HEADERS += \
    myclassicclass.h \
    myqclass.h
