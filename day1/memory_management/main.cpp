#include <QCoreApplication>
#include "myclassicclass.h"
#include "myqclass.h"
#include <iostream>
#include <QObject>
#include <memory>


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
//    MyClassicClass one("Leszek");
//    MyClassicClass *two =
//            new MyClassicClass("Other");
//    one.do_something();
//    two->do_something();
//    delete two;

    QObject parent;
    MyQClass *one = new MyQClass("Leszek", &parent);
    MyQClass *two = new MyQClass("Other", &parent);

    // or

    std::unique_ptr<MyClassicClass> three
            = std::make_unique<MyClassicClass>("MyClassicClass test");
    three->do_something();

    std::unique_ptr<MyClassicClass> four
            = std::unique_ptr<MyClassicClass>(new MyClassicClass(QString("test2")));

    char c;
    std::cin >> c;
    return 0;
}
