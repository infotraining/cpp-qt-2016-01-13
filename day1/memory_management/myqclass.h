#ifndef MYQCLASS_H
#define MYQCLASS_H

#include <QString>
#include <QObject>

class MyQClass : public QObject
{
    Q_OBJECT

    QString text_;
public:
    explicit MyQClass(QString text, QObject *parent = 0);
    ~MyQClass();

signals:

public slots:
    void do_something();
};

#endif // MYQCLASS_H
