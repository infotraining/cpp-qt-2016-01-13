#include "myqclass.h"
#include <QDebug>

MyQClass::MyQClass(QString text, QObject *parent)
    : text_(text), QObject(parent)
{
    qDebug() << "ctor of " << text_;

}

MyQClass::~MyQClass()
{
    qDebug() << "dtor of " << text_;

}

void MyQClass::do_something()
{
    qDebug() << "hello " << text_;

}
