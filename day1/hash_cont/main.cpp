#include <QCoreApplication>
#include <QHash>
#include <QDebug>
#include <QString>
#include <QtMath>

class Point
{
public:
    int x_,y_;

    Point(int x, int y) : x_(x), y_(y)
    {
    }

    double dist()
    {
        return( qSqrt(x_ * x_ + y_ * y_));
    }

    bool operator==(const Point& p2) const
    {
        return (x_ == p2.x_ && y_ == p2.y_);
    }

};


uint qHash(Point p)
{
    return p.x_ + p.y_*100;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QHash<QString, int> h;
    h["Polska"] = 38;
    h.insert("Angola", 42);
    h.insert("Niemcy", 80);
    h.insert("Angola", 12);
    qDebug() << h;

    QHash<Point, double> mapa;
    mapa.insert(Point(1,1), 10.0);
    mapa.insert(Point(2,2), 11.0);


    return a.exec();
}
