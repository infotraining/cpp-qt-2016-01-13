#include <QCoreApplication>
#include <QString>
#include <QDebug>
#include <iostream>
#include <string>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString str1("ala");

    QString test1("żółw");
    std::string test2("żółw");
    qDebug() << test1.length();
    qDebug() << test2.length();
    //qDebug() << test1.toUpper();
    QString num = QString::number(12.34);
    qDebug() << num;
    qDebug() << num.toDouble();

    QString filename("myfile.txt");
    if (filename.endsWith(".txt"))
    {
        qDebug() << "txt file!!";
    }

    QString str2("Ala ma kota");
    qDebug() << str2.split(QRegExp("\\W"));

    QString templ("Ala ma %2 i %1");
    qDebug() << templ.arg("psa", "kota", "a",
                          "b", "c");

    qDebug() << str1;
    std::cout << str1.toStdString() << std::endl;

    return a.exec();
}
