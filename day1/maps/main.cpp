#include <QCoreApplication>
#include <QMap>
#include <QString>
#include <QDebug>
#include <vector>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QMap<QString, int> mapa;
    mapa.insert("Polska", 38);
    mapa.insert("Angola", 42);
    mapa.insert("Niemcy", 80);
    mapa.insert("Angola", 12);
    mapa.insertMulti("Angola", 100);
    mapa["Brytania"] = 70;
    mapa["Angola"] = 19;
    qDebug() << mapa["Polska"];
    qDebug() << mapa["Angola"];
    qDebug() << mapa;
    qDebug() << mapa.values();
    qDebug() << mapa.keys();

    QMapIterator<QString, int> it(mapa);
    while(it.hasNext())
    {
        QMap<QString, int>::ConstIterator elem = it.next();
        qDebug() << elem.key() << elem.value();
    }

    qDebug() << "-------------";

    //QMap<QString, int>::const_iterator sit = mapa.constBegin();
    for(auto sit = mapa.constBegin(); sit != mapa.constEnd(); ++sit)
    {
        qDebug() << sit.key() << sit.value();
    }

    for(auto el : mapa) // broken by design...
    {
        qDebug() << el;
    }

    std::vector<int> v {1,2,3};
    //std::vector<int>::const_iterator vit;
    for( auto vit = std::cbegin(v) ; vit != std::cend(v) ; ++vit)
    {
        qDebug() << *vit;
    }

    return a.exec();
}
