#include <QCoreApplication>
#include <QList>
#include <QString>
#include <QStringList>
#include <QDebug>
#include <QTime>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QList<QString> list;
    for (int i = 0 ; i < 100 ; ++i)
        list << "Ala" << "Ola" << "Ela";
    //qDebug() << list;

    QString temp;

    QTime timer;


    //QList<QString>::Iterator itstl;
    timer.start();
    for (int i = 0 ; i < 10000 ; ++i)
    {
        for(auto itstl = list.cbegin() ; itstl != list.cend() ; ++itstl)
        {
            temp =  *itstl;
        }
    }
    qDebug() << "stl style = " << timer.elapsed();

    timer.start();
    for (int i = 0 ; i < 10000 ; ++i)
    {
        foreach(QString el, list)
        {
            temp =  el;
        }
    }
    qDebug() << "foreach style = " << timer.elapsed();

    for(int i = 0 ; i < list.size() ; ++i)
    {
        temp =  list.at(i);
    }

    timer.start();
    for (int i = 0 ; i < 10000 ; ++i)
    {
        for(const auto& el : list)
        {
            temp = el;
        }
    }
    qDebug() << "c++11 style = " << timer.elapsed();

    timer.start();
    for (int i = 0 ; i < 10000 ; ++i)
    {
        QListIterator<QString> it(list);
        while(it.hasNext())
        {
            temp = it.next();
        }
    }
    qDebug() << "java style = " << timer.elapsed();


    return a.exec();
}
