#include "entrydialog.h"
#include "ui_entrydialog.h"

EntryDialog::EntryDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EntryDialog)
{
    ui->setupUi(this);
}

EntryDialog::~EntryDialog()
{
    delete ui;
}

QString EntryDialog::getName()
{
    return ui->lineEditName->text();
}

QString EntryDialog::getPhone()
{
    return ui->lineEditPhone->text();
}

void EntryDialog::setName(QString val)
{
    ui->lineEditName->setText(val);
}

void EntryDialog::setPhone(QString val)
{
    ui->lineEditPhone->setText(val);
}
