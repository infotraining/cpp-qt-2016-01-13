#ifndef PBWINDOW_H
#define PBWINDOW_H

#include <QWidget>
#include <QString>

namespace Ui {
class PBWindow;
}

class PBWindow : public QWidget
{
    Q_OBJECT

public:
    explicit PBWindow(QWidget *parent = 0);
    ~PBWindow();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::PBWindow *ui;
};

#endif // PBWINDOW_H
