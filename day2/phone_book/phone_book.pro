#-------------------------------------------------
#
# Project created by QtCreator 2016-01-14T13:23:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = phone_book
TEMPLATE = app


SOURCES += main.cpp\
        pbwindow.cpp \
    entrydialog.cpp \
    mylabel.cpp

HEADERS  += pbwindow.h \
    entrydialog.h \
    mylabel.h

FORMS    += pbwindow.ui \
    entrydialog.ui \
    mylabel.ui
