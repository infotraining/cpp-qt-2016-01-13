#include "pbwindow.h"
#include "entrydialog.h"
#include "ui_pbwindow.h"
#include "mylabel.h"
#include <QDebug>
#include <QString>

PBWindow::PBWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PBWindow)
{
    ui->setupUi(this);
}

PBWindow::~PBWindow()
{
    delete ui;
}

void PBWindow::on_pushButton_clicked()
{
    EntryDialog dlg(this);
    if (dlg.exec() == QDialog::Accepted)
    {
        qDebug() << "done";
        QListWidgetItem* item = new QListWidgetItem();//QString("%1 -- %2").arg(dlg.getName(), dlg.getPhone()));
        MyLabel* label = new MyLabel;
        item->setSizeHint(QSize(0, 50));
        ui->listWidget->addItem(item);
        ui->listWidget->setItemWidget(item, label);

    }
}

void PBWindow::on_pushButton_2_clicked()
{
    QStringList params = ui->listWidget->currentItem()->text().split(" -- ");
    EntryDialog dlg(this);
    if(params.size() == 2)
    {
        dlg.setName(params.at(0));
        dlg.setPhone(params.at(1));
    }
    if (dlg.exec() == QDialog::Accepted)
    {
        ui->listWidget->currentItem()->setText(QString("%1 -- %2").arg(dlg.getName(), dlg.getPhone()));
    }
}

void PBWindow::on_pushButton_3_clicked()
{
    for(auto item : ui->listWidget->selectedItems())
    {
        delete item;
    }
}
