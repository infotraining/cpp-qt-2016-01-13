#ifndef COUNTER_H
#define COUNTER_H

#include <QObject>
#include <QString>

class Counter : public QObject
{
    Q_OBJECT
    int value_;
    QString name_;
public:
    explicit Counter(QString name, QObject *parent = 0);
    int value() const;

signals:
    void valueChanged(int value, QString name);

public slots:
    void setValue(int value);
};

#endif // COUNTER_H
