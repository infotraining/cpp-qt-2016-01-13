#ifndef OBSERVER_H
#define OBSERVER_H

#include <QObject>
#include "counter.h"

class Observer : public QObject
{
    Q_OBJECT
public:
    explicit Observer(QObject *parent = 0);
    void registerCounter(Counter *counter);

signals:

public slots:
    void logToDebug(int value, QString name);
};

#endif // OBSERVER_H
