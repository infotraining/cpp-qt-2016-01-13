#include <QCoreApplication>
#include "counter.h"
#include "observer.h"
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Counter one("one");
    Counter two("two");
    one.setValue(100);
    //two.setValue(200);

    Observer obs1;
    obs1.registerCounter(&one);
    obs1.registerCounter(&two);

    qDebug() << "one" << one.value();
    qDebug() << "two" << two.value();
    qDebug() << "-----------------------";
//    QObject::connect(&one, SIGNAL(valueChanged(int, QString)),
//                     &two, SLOT(setValue(int)));

    // Qt5 new connect semantic
    QObject::connect(&one, &Counter::valueChanged,
                     &two, &Counter::setValue);

    QObject::connect(&two, SIGNAL(valueChanged(int, QString)),
                     &one, SLOT(setValue(int)));

    one.setValue(300);
    qDebug() << "one" << one.value();
    qDebug() << "two" << two.value();

    two.setValue(1100);
    qDebug() << "-----------------------";
    qDebug() << "one" << one.value();
    qDebug() << "two" << two.value();
    return a.exec();
}
