#include "counter.h"

Counter::Counter(QString name, QObject *parent)
    : QObject(parent), name_(name)
{

}

int Counter::value() const
{
    return value_;
}

void Counter::setValue(int value)
{
    if (value != value_)
    {
        value_ = value;
        emit valueChanged(value_, name_);
    }
}
