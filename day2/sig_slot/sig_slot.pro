QT += core
QT -= gui

CONFIG += c++11

TARGET = sig_slot
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    counter.cpp \
    observer.cpp

HEADERS += \
    counter.h \
    observer.h
