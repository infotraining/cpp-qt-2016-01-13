#include "observer.h"
#include <QDebug>

Observer::Observer(QObject *parent) : QObject(parent)
{

}

void Observer::registerCounter(Counter *counter)
{
//    connect(counter, &Counter::valueChanged,
//            this, &Observer::logToDebug);
    connect(counter, &Counter::valueChanged,
            [] (int v, QString n) {
                qDebug() << n << " lambda " << v;});
}

void Observer::logToDebug(int value, QString name)
{
    qDebug() << name << "has changed to: " << value;
}
