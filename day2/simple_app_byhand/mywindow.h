#ifndef MYWINDOW_H
#define MYWINDOW_H

#include <QWidget>
#include <QDial>
#include <QSpinBox>
#include <QPushButton>

class MyWindow : public QWidget
{
    Q_OBJECT    

    QDial* dial;
    QPushButton *btn;
    QSpinBox *spinbox;

public:
    MyWindow(QWidget *parent = 0);
    ~MyWindow();
};

#endif // MYWINDOW_H
