#include "mywindow.h"
#include <QHBoxLayout>
#include <QVBoxLayout>


MyWindow::MyWindow(QWidget *parent)
    : QWidget(parent)
{
    dial = new QDial();
    btn = new QPushButton();
    spinbox = new QSpinBox();

    QHBoxLayout *bottom_layout = new QHBoxLayout();
    bottom_layout->addWidget(spinbox);
    bottom_layout->addWidget(btn);

    QVBoxLayout *main_layout = new QVBoxLayout();
    main_layout->addWidget(dial);
    main_layout->addLayout(bottom_layout);

    this->setLayout(main_layout);

    btn->setText("E&xit");

    connect(dial, SIGNAL(valueChanged(int)),
            spinbox, SLOT(setValue(int)));

    connect(spinbox, SIGNAL(valueChanged(int)),
            dial, SLOT(setValue(int)));

    connect(btn, SIGNAL(clicked(bool)),
            this, SLOT(close()));

}

MyWindow::~MyWindow()
{

}
