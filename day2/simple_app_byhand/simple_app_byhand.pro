#-------------------------------------------------
#
# Project created by QtCreator 2016-01-14T11:03:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = simple_app_byhand
TEMPLATE = app


SOURCES += main.cpp\
        mywindow.cpp

HEADERS  += mywindow.h
