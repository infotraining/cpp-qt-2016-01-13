#include "widget.h"
#include "ui_widget.h"
#include <QFileDialog>
#include <QString>
#include <QDebug>
#include <QTextStream>
#include <QFile>
#include <QHash>
#include <QMap>
#include <QTime>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    connect(ui->plainTextEdit, SIGNAL(textChanged()),
            this, SLOT(calculate()));
    connect(ui->leFilter, SIGNAL(textChanged()),
            this, SLOT(calculate()));
    connect(ui->leSplit, SIGNAL(textChanged()),
            this, SLOT(calculate()));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_Load_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    "Open file for concordance",
                                                    ".",
                                                    "All files (*.*);;Text (*.txt)");

    qDebug() << filename;
    if (!filename.isEmpty())
    {
        QFile f(filename);
        if (f.open(QFile::ReadOnly | QFile::Text))
        {
            QTextStream in(&f);
            ui->plainTextEdit->setPlainText( f.readAll() );
        }
    }
    //calculate();
}

void Widget::calculate()
{
    ui->tableWidget->clearContents();
    QTime timer;
    timer.start();
    QString split_txt = ui->leSplit->text();
    QString filter_txt = ui->leFilter->text();
    QStringList words =
           ui->plainTextEdit->toPlainText().split(QRegExp(split_txt)).filter(QRegExp(filter_txt));
    QHash<QString, int> hfreq;
    for(const auto& word : words)
    {
        hfreq[word.toLower()]++;
    }
    qDebug() << "QHash Time = " << timer.elapsed() << " ms";

    QMap<int, QStringList> result;
    QHashIterator<QString, int> it(hfreq);
    while(it.hasNext())
    {
        it.next();
        result[it.value()] << it.key();
    }

    //qDebug() << result;

    QMapIterator<int, QStringList> rit(result);
    rit.toBack();
    int i = 0;
    ui->tableWidget->setRowCount(30);
    while(rit.hasPrevious() && i < 30)
    {
        ++i;
        rit.previous();
        //qDebug() << i << rit.key() << rit.value();
        ui->tableWidget->setItem(i, 0, new QTableWidgetItem(QString::number(rit.key())));
        ui->tableWidget->setItem(i, 1, new QTableWidgetItem(rit.value().join(", ")));
    }
}
